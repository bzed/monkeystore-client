#!/usr/bin/python

from distutils.core import setup
import platform

version = "0.1.5.5"
description = "minimal monkeystore client"
name = "monkeystore-client"


if platform.system() == 'Windows':
    import py2exe

    setup (
        version = version,
        description = description,
        name = name,
        console = ["monkeystore"],
        zipfile = "shared.lib",
        options={"py2exe": {"includes": ["psutil"]}}
    )

else:
    setup (
        version = version,
        description = description,
        name = name,
        scripts = ["monkeystore"]
    )
